<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ParsedData;
use Symfony\Component\DomCrawler\Crawler;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;


class parse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $month = array(
            1=>'января',2=>'февраля',3=>'марта',4=>'апреля',5=>'мая',6=>'июня',7=>'июля',8=>'августа',9=>'сентября',10=>'октября',11=>'ноября',12=>'декабря'
        );
        $numMonth = array(
            'января'=>1,'февраля'=>2,'марта'=>3,'апреля'=>4,'мая'=>5,'июня'=>6,'июля'=>7,'августа'=>8,'сентября'=>9,'октября'=>10,'ноября'=>11,'декабря'=>12
        );
        $mainUrl = 'https://gkb81.ru/sovety/';
        $startpage = file_get_contents($mainUrl); //можно по старинке
        preg_match_all('/(https:\/\/gkb81\.ru\/sovety\/page\/\d+\/)/', $startpage,$pages);//получаем ссылки на пагинацию
        $pages[0][] = $mainUrl; //добавляем первую страницу в массив
        $pages = array_unique($pages[0]); //удаляем дубли
        asort($pages);//и сортируем
        foreach($pages as $page){
            $pageContent = file_get_contents($page);
            preg_match_all("/<a href=\"(https:\/\/gkb81\.ru\/sovety\/[a-zA-Z0-9\-]*\/)\" class=\"header\"><h3>([a-zA-ZА-Яа-я0-9\-\:\s\?\,\–]|.*)<\/h3>/",$pageContent, $articles); //и даже так
            foreach($articles[1] as $k=>$articleLink){
                if(!ParsedData::where('name',$articles[2][$k])->exists()){ //проверяем, есть ли эта запись
                    $html = Http::get($articleLink); // пакет лары искаропки
                    //echo $html;
                    $articlePage = new Crawler(null, $articleLink); //пакет симфони. Можно ведь и проще чем с регулярками
                    $articlePage->addHtmlContent($html, 'UTF-8');
                    $name = $articlePage->filter("header.header-h1 > div.container > h1")->text();//заголовок статьи
                    $thumb = $articlePage->filter("img.attachment-holamed-post.size-holamed-post.wp-post-image")->attr('src');//получаем урл тумбы
                    $ext = pathinfo($thumb, PATHINFO_EXTENSION); //расширение тумбы
                    preg_match('/https:\/\/gkb81\.ru\/sovety\/([a-zA-Z0-9\-]*)/',$articleLink,$localthubname);//локальное имя тумбы по урлу поста
                    $fulltext = $articlePage->filter(".description .text.text-page")->html();//получаем текст статьи
                    $fulltext = trim(strip_tags($fulltext,"<p><ul><li><i><b><strong><h1><h2><h3><h4><h5><a><span>"));//вырезаем лишние теги из текста статьи
                    $date = $articlePage->filter("span.ltx-date span.dt")->text(); //получаем дату
                    $date = str_replace($month,$numMonth, str_replace(",",'',str_replace(" ",".",$date))); //вырезаем весь шлак из даты
                    $published_date = date("Y-m-d H:i:s",strtotime($date)); //и приводим к нормальному виду
                    $pathToFile = Storage::put('public/'.$localthubname[1].".".$ext, $thumb, 'public');

                    $article = new ParsedData();
                    $article->name = $name;
                    $article->link = $articleLink;
                    $article->fulltext = $fulltext;
                    $article->published_date = $published_date;
                    $article->thumb = $thumb;
                    $article->localthumb = $pathToFile;
                    $article->save();
                } else {
                    echo '=================='.PHP_EOL;
                    echo "Новость с названием \"{$articles[2][$k]}\" ранее синхронизирована. Пропускаем.".PHP_EOL;
                    echo '=================='.PHP_EOL;
                }
            }
        }
        return 0;

    }
}