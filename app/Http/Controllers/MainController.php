<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Process\Process;
use Artisan; 


class MainController extends Controller
{
    //
    public function index(){
        // print_r( dir(__DIR__)) ;
        // $process = new Process(['php ' . base_path('artisan') . ' command:parse &'], null,null,null,0);
        // $process->setTimeout(0);
        // $result = $process->start(); 
        $result = Artisan::queue('command:parse');
        return "Процесс запущен: ";
    }  
}
